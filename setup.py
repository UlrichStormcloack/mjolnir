from setuptools import setup

NAME = "vhe_render"
DESCRIPTION = 'Renders UE4 Movie | converts to .mov | pushes to perforce'
URL = 'https://gitlab.com/UlrichStormcloack/mjolnir'
EMAIL = 'atamanpopo@gmail.com'
AUTHOR = 'Roman Volodin'
REQUIRES_PYTHON = '>=2.7.0'
VERSION = '0.5'



setup(
   name=NAME, 
   version=VERSION,
   description=DESCRIPTION,
   author=AUTHOR,
   author_email=EMAIL,
   url=URL,
   packages=['render'],  #same as name
   license='MIT',
   # install_requires=['bar', 'greek'], #external packages as dependencies
   dependency_links = ['git+https://gitlab.com/UlrichStormcloack/mjolnir.git#egg=mjolnir']

)